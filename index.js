function compare(a, b) {
  if (isNaN(a) || isNaN(b)) {
    return "Need numerical values";
  } else if (a > b) {
    return 1;
  } else if (a < b) {
    return -1;
  } else if (a === b) {
    return 0;
  }
}
